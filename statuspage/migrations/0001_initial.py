# Generated by Django 2.2.5 on 2019-11-04 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dates', models.DateTimeField(auto_now_add=True)),
                ('status', models.TextField(max_length=300)),
            ],
        ),
    ]
