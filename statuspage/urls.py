from django.urls import path
from . import views

app_name = 'statuspage'

urlpatterns = [
   path('', views.home, name = 'home'),
   path('add_status/', views.add_status, name = 'add_status')
]