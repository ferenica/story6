from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import Status
from .forms import StatusForm
from datetime import date
import time


# Create your tests here.
class Story6_Web_Test(TestCase):
    
    def test_story6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_story6_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_halo(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

    def test_form_is_exist(self):
        response = Client().get('')
        self.assertIn('</form>', response.content.decode())

class Story6_Form_Test(TestCase):
    def test_forms_valid(self):
        form_data = {'status': 'sebuah statusss'}
        form = StatusForm(data = form_data)
        self.assertTrue(form.is_valid())
    
    # def test_forms_not_valid(self):
    #    form_data = {'status': 'status bagaimana ini'}
    #    form = StatusForm(data = form_data)
    #    self.assertFalse(form.is_valid())

class Story6_Model_Test(TestCase):
    def test_model_create_new_status(self):
        new_status = Status.objects.create(dates = timezone.now(), status = 'sebuah status')
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)
    
    # def test_save_POST_request(self):
    #    response = self.client.post('/story-6/add_status/', data = {'dates' : '2019-10-4T15:16', 'status' : 'sedang tidur'})
    #    counting_status_object = Status.object.all().count()
    #    self.assertEqual(counting_status_object, 1)

    #    self.assertEqual(response.status_code, 302)

    #    new_response = self.client.get('/story-6/')
    #    html_response = new_response.content.decode('utf8')
    #    self.assertIn('sedang tidur', html_response)

class Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(Functional_Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Functional_Test, self).tearDown()
    
    def test_input_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        status.send_keys('sebuah status')

        submit.click()