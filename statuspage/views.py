from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
from datetime import datetime, date

# Create your views here.
def home(request):
    data = Status.objects.all()
    form = StatusForm()
    content = {'title' : 'statusss',
            'form' : form,
            'text' : 'Halo, apa kabar?',
            'data' : data}
    return render(request, "status.html", content)

def add_status(request):
    form = StatusForm(request.POST)
    if form.is_valid():
        form.save()
    return redirect('/')
